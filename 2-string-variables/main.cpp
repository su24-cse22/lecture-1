#include <iostream>
using namespace std;

int main() {

    /*
    
        Create a program that outputs the following information
        using string variables.

        Name:  Rufus Bobcat
        Role:  University Mascot
        Email: rbobcat@ucmerced.edu
        Phone: 209-123-4567

    */

    string name = "Rufus Bobcat";
    string role = "University Mascot";
    string email = "rbobcat@ucmerced.edu";
    string phone = "209-123-4567";


    cout << "Name:  " << name << endl;
    cout << "Role:  " << role << endl;
    cout << "Email: " << email << endl;
    cout << "Phone: " << phone << endl;

    return 0;
}