#include <iostream>
using namespace std;

int main() {

    /*
    
        Create a program that outputs the following report
        card. Use the Linux command line to save the 
        program output to a file called "report-card.txt".

        .-----------------------.
        | REPORT CARD           |
        | Name: Rufus Bobcat    |
        |-----------------------|
        | Reading: B            |
        | Writing: B            |
        | Math:    A            |
        | Science: A            |
        | History: C            |
        | Art:     A            |
        | P.E:     A            |
        |-----------------------|
        |           Credits: 35 |
        |             GPA: 3.25 |
        .-----------------------.

    */

    cout << ".-----------------------." << endl; 
    cout << "| REPORT CARD           |" << endl;
    cout << "| Name: Rufus Bobcat    |" << endl;
    cout << "|-----------------------|" << endl;
    cout << "| Reading: B            |" << endl;
    cout << "| Writing: B            |" << endl;
    cout << "| Math:    A            |" << endl;
    cout << "| Science: A            |" << endl;
    cout << "| History: C            |" << endl;
    cout << "| Art:     A            |" << endl;
    cout << "| P.E:     A            |" << endl;
    cout << "|-----------------------|" << endl;
    cout << "|           Credits: 35 |" << endl;
    cout << "|             GPA: 3.25 |" << endl;
    cout << ".-----------------------." << endl;

    return 0;
}