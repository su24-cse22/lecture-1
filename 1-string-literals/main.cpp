#include <iostream>
using namespace std;

int main() {

    /*

        Create a program that outputs the following information
        using string literals.

        Welcome to CSE 22!
        In this course, you will learn to program using C++.
        
    */

    cout << "Welcome to CSE 22!" << endl;
    cout << "In this course, you will learn to program using C++." << endl;

    return 0;
}