#include <iostream>
using namespace std;

int main() {

    /*
    
        Create a program that outputs the following information using
        the appropriate variable types.

        Name:           Carbon
        Symbol:         C
        Atomic Number:  6
        Atomic Mass:    12.011
        Chemical Group: Nonmetal

    */

    string name = "Carbon";
    string symbol = "C";
    int atomicNumber = 6;
    float atomicMass = 12.011;
    string chemicalGroup = "Nonmetal";

    cout << "Name:           " << name << endl;
    cout << "Symbol:         " << symbol << endl;
    cout << "Atomic Number:  " << atomicNumber << endl;
    cout << "Atomic Mass:    " << atomicMass << endl;
    cout << "Chemical Group: " << chemicalGroup << endl;


    return 0;
}